//
//  ViewController.swift
//  Login Segue
//
//  Created by Keo Shiko on 2021/01/27.
//  Copyright © 2021 Glucode. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var username: UITextField!
    
    var name : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender:
    Any?) {
        segue.destination.navigationItem.title = username.text
        
        
        if segue.identifier == "show" {
            print("we init")
            
            guard let resetVC = segue.destination as? LoginViewController else {return}
            
            guard let name = username.text else { return  }
            
            resetVC.passedName = name
        }
    }
    
}

