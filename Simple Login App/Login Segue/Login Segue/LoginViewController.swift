//
//  LoginViewController.swift
//  Login Segue
//
//  Created by Keo Shiko on 2021/01/28.
//  Copyright © 2021 Glucode. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    
    var passedName : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let val = passedName {
            nameLabel.text = val
        }
        

    
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
