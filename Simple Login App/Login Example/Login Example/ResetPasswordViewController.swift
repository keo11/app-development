//
//  ResetPasswordViewController.swift
//  Login Example
//
//  Created by Keo Shiko on 2021/01/27.
//  Copyright © 2021 Glucode. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {

    
    @IBOutlet weak var resetLabel: UILabel!
    
    var password : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()


        
        if let pass = password {
            resetLabel.text = pass
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
