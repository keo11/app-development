//
//  ViewController.swift
//  Login Example
//
//  Created by Keo Shiko on 2021/01/25.
//  Copyright © 2021 Glucode. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var username: UITextField!
    var name : String?
    
    @IBOutlet weak var loginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        loginButton.isEnabled = false
        loginButton.isHidden = true
        
        username.delegate = self
        passwordField.delegate = self
    }


    @IBAction func unwindToLogin(unwindSegue: UIStoryboardSegue) {
    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender:
    Any?) {
        segue.destination.navigationItem.title = username.text
        
        
        
        if segue.identifier == "loginNavController" {
            print("yes")
            segue.destination.navigationItem.title = username.text
        }
        
        if segue.identifier == "reset" {
            print("in reset")
            //guard let text = username.text else {return}
            
            guard let resetVC = segue.destination as? ResetPasswordViewController else {return}
            
            guard let name = username.text else { return  }
            
            resetVC.password = name
        }
    }
    
    func checkField() {
        if username.text?.count ?? 0 > 3 && passwordField.text?.count ?? 0 > 3 {
            loginButton.isEnabled = true
            loginButton.isHidden = false
        }
        else {
            loginButton.isEnabled = false
            loginButton.isHidden = true
        }
    }
    
   /* func textFieldDidBeginEditing(_ textField: UITextField) {
        checkField()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        checkField()
    }*/

    func textFieldDidChangeSelection(_ textField: UITextField) {
        checkField()
    }
    
}

