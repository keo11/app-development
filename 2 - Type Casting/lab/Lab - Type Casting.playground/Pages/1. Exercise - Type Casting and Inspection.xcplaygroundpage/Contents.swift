/*:
 ## Exercise - Type Casting and Inspection
 
 Create a collection of type [Any], including a few doubles, integers, strings, and booleans within the collection. Print the contents of the collection.
 */
let types = ["Name" , 12.5, true , 30, ] as [Any]

/*:
 Loop through the collection. For each integer, print "The integer has a value of ", followed by the integer value. Repeat the steps for doubles, strings and booleans.
 */
for item in types {
    if let num = item as? Int {
        print("The integer has a value of \(num)")
    }
}

/*:
 Create a [String : Any] dictionary, where the values are a mixture of doubles, integers, strings, and booleans. Print the key/value pairs within the collection
 */
let info : [String : Any] = ["Name" : "Keo", "Age" : 30, "Human being" : true, "Height" : 180.50]

/*:
 Create a variable `total` of type `Double` set to 0. Then loop through the dictionary, and add the value of each integer and double to your variable's value. For each string value, add 1 to the total. For each boolean, add 2 to the total if the boolean is `true`, or subtract 3 if it's `false`. Print the value of `total`.
 */
var total = 0.0

for item in info {
    if let integer = item.value as? Int {
        total += Double(integer)
    }
    if let dub = item.value as? Double {
        total += dub
    }
    if let str = item.value as? String {
        total += 1
    }
    if let boo = item.value as? Bool {
        if boo{
            total += 2
        }
        else {
            total -= 3
        }
    }
}

print(total)
/*:
 Create a variable `total2` of type `Double` set to 0. Loop through the collection again, adding up all the integers and doubles. For each string that you come across during the loop, attempt to convert the string into a number, and add that value to the total. Ignore booleans. Print the total.
 */
var total2 : Double = 0


for item in info {
    if let integer = item.value as? Int {
        total2 += Double(integer)
    }
    if let dub = item.value as? Double {
        total2 += dub
    }
    if let str = item.value as? String {
        if let dub = Double(str) {
            total2 += dub
        }
    }
}

print(total2)
//: page 1 of 2  |  [Next: App Exercise - Workout Types](@next)
