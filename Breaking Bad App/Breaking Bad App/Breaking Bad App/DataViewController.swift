//
//  DataViewController.swift
//  Breaking Bad App
//
//  Created by Keo Shiko on 2021/02/16.
//  Copyright © 2021 Glucode. All rights reserved.
//

import UIKit

class DataViewController: UIViewController {
    
    
    
    var imageCacheDictionary : Dictionary<String, UIImage> = [String : UIImage]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //loadingViewController.addLoadingView()

        // Do any additional setup after loading the view.
    }
    
        // Create Date Formatter
    let dateFormatter = DateFormatter()
    
    //a list of all of the characters
    var getCharacters = [Characters]()
    
    
    func getAge( convert : String) -> Int {
        
        // Set Date Format
        dateFormatter.dateFormat = "MM-dd-yy"

        
        // Convert String to Date
        if let newDate = dateFormatter.date(from: convert) {
            
            let now = Date()
            let birthday: Date = newDate
            let calendar = Calendar.current
            
            let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
            
            return ageComponents.year!
        }
        
        return 0
    }
    
    //function to decode the JSON into the characters array
    func getAPIobjects(completed: @escaping () -> () ) {
        
        let url = URL(string: "https://www.breakingbadapi.com/api/characters")
        
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error == nil {
                do {
                    self.getCharacters = try JSONDecoder().decode([Characters].self, from: data!)
                    
                    
                    DispatchQueue.main.async {
                        completed()
                        
                    }
                }catch {
                    print("Error with JSON \(error.localizedDescription)")
                }
            }
            else {
                print(error?.localizedDescription as Any)
            }
        }.resume()
        
        
    
    }
    

    
}

//extension of the image view to load the images from the API
extension UIImageView {
    func loadImages(urlString : String) {
        
        if let image = imageCache.object(forKey: urlString as NSString) {
            self.image = image
            return
        }
        
        guard let url = URL(string: urlString) else {
            return
        }
        
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let unwrappedImage = UIImage(data: data) {
                    
                    imageCache.setObject(unwrappedImage, forKey: urlString as NSString)
                    
                    DispatchQueue.main.async {
                        self?.image = unwrappedImage
                    }
                }
            }
            
        }
    }
}

//global variable for image cache
var imageCache = NSCache<NSString, UIImage>()

// extend functionality to handle empty data
extension UITableView {

    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()

        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }

    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
    
    func setLoadingMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .gray
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 30)
        messageLabel.sizeToFit()

        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }
    
    func addLoadingView() {
        let view = UIView()
        
        
        
        view.backgroundColor = UIColor(white: 0, alpha: 0.7)
        
        let spinner = UIActivityIndicatorView(style: .large)
        
                spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.startAnimating()
        

        
        view.addSubview(spinner)
        
        spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
    }
}
