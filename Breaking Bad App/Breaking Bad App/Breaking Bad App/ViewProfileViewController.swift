//
//  ViewProfileViewController.swift
//  Breaking Bad App
//
//  Created by Keo Shiko on 2021/02/12.
//  Copyright © 2021 Glucode. All rights reserved.
//

import UIKit

class ViewProfileViewController : UIViewController {
    
    @IBOutlet weak var displayImage: UIImageView!
    
    @IBOutlet weak var nicknameLab: UILabel!
    
    @IBOutlet weak var dobLabel: UILabel!
    
    @IBOutlet weak var portrayedLabel: UILabel!
    @IBOutlet weak var occupationLabel: UILabel!
    
    var profile : Characters?

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nicknameLab.text = profile!.nickname
        dobLabel.text = profile!.dobString
        portrayedLabel.text = profile!.portrayed
        displayImage.loadImages(urlString: profile!.image)
        occupationLabel.text = ""
        for char in profile!.occupation {
            //if index(ofAccessibilityElement: char) < profile!.occupation.count {
                occupationLabel.text?.append("-" + char + "\n")
            //} else {
            //    occupationLabel.text?.append("-" + char)
            //}
        }
        
    }
    
    
}
