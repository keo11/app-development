//
//  LoadingViewController.swift
//  Breaking Bad App
//
//  Created by Keo Shiko on 2021/02/26.
//  Copyright © 2021 Glucode. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        loadView()
        
        // Do any additional setup after loading the view.
    }
    
    
    override func loadView() {
        view = UIView()
        
        
        
        view.backgroundColor = UIColor(white: 5, alpha: 1.0)
        
        let spinner = UIActivityIndicatorView(style: .large)
        
        spinner.translatesAutoresizingMaskIntoConstraints = false
        
        spinner.startAnimating()
        
        
        
        view.addSubview(spinner)
        
        spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
    }
    
    func addLoadingView() {
        let view = UIView()
        
        
        
        view.backgroundColor = UIColor(white: 5, alpha: 1.0)
        
        let spinner = UIActivityIndicatorView(style: .large)
        
        spinner.translatesAutoresizingMaskIntoConstraints = false
        
        spinner.startAnimating()

        
        view.addSubview(spinner)
        
        spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        
    }

}
