//
//  ProfileTableViewCell.swift
//  Breaking Bad App
//
//  Created by Keo Shiko on 2021/02/09.
//  Copyright © 2021 Glucode. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {
    
    static let identifier = "ProfileTableViewCell"
    static func nib() -> UINib {
        return UINib(nibName: "ProfileTableViewCell", bundle: nil)
    }
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var nicknameLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var profileImage: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setLabels(character : Characters) {
        self.nameLabel.text = String(describing: character.name)
         
         self.dateLabel.text = String(describing: character.dobString)
         
         self.nicknameLabel.text = String(describing: character.nickname)
         
         self.profileImage.loadImages(urlString: character.image)
    }
    
}




