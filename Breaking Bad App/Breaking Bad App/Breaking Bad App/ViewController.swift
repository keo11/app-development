//
//  ViewController.swift
//  Breaking Bad App
//
//  Created by Keo Shiko on 2021/02/09.
//  Copyright © 2021 Glucode. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var apiObjects = DataViewController()
    var loadingViewController = LoadingViewController()
    
    @IBOutlet weak var table: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        table.register(ProfileTableViewCell.nib(), forCellReuseIdentifier: ProfileTableViewCell.identifier)
        table.delegate = self
        table.dataSource = self
        
        //addloadingIndicator()
        //self.table.setEmptyMessage("⚠️")
        
        
        
        apiObjects.getAPIobjects {
            self.table.restore()
            self.table.reloadData()
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        /*if apiObjects.getCharacters.count == 0 {
            self.table.setEmptyMessage("⚠️ ERROR - no data to display")
        } else {
            self.table.restore()
        }*/
        
        return apiObjects.getCharacters.count == 0 ? 1 : apiObjects.getCharacters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        if apiObjects.getCharacters.count == 0 {
            let loaderCell = tableView.dequeueReusableCell(withIdentifier: "loadingCell", for: indexPath)
            
            return loaderCell
        }
        else {
            let character = apiObjects.getCharacters[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
            
            cell.setLabels(character: character)
            
            return cell
        }
        

        
        
        

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "viewProfile", sender: self)
    }
    
    //Send data through the segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ViewProfileViewController {
            
            let character = apiObjects.getCharacters[(table.indexPathForSelectedRow?.row)!]
            
            destination.profile = character
            
            destination.navigationItem.title = character.name
            
        }
    }
    
    func addloadingIndicator()  {

        // add the spinner view controller
        addChild(loadingViewController)
        loadingViewController.view.frame = view.frame
        view.addSubview(loadingViewController.view)
        loadingViewController.didMove(toParent: self)

        // wait five seconds to simulate some work happening
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            // then remove the spinner view controller
            self.loadingViewController.willMove(toParent: nil)
            self.loadingViewController.view.removeFromSuperview()
            self.loadingViewController.removeFromParent()
        }
    }
    
}

