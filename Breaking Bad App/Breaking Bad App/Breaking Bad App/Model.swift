//
//  Model.swift
//  Breaking Bad App
//
//  Created by Keo Shiko on 2021/02/10.
//  Copyright © 2021 Glucode. All rights reserved.
//

import Foundation


protocol CharacterDetails {
    var name : String { get }
    var nickname : String { get }
    var birthday : String { get }
    
}

struct Characters : Codable, CharacterDetails {
    
    
    enum CodingKeys: String, CodingKey {
        case name, birthday, nickname, portrayed, occupation
        
        case image = "img"
    }
    
    var name : String
    var birthday : String
    var occupation : [String]
    var image : String
    var nickname : String
    var portrayed : String
    
    var age : String {
        
            // Create Date Formatter
        let dateFormatter = DateFormatter()
        
        // Set Date Format
        dateFormatter.dateFormat = "MM-dd-yy"

        
        // Convert String to Date
        if let newDate = dateFormatter.date(from: birthday) {
            
            let now = Date()
            let birthday: Date = newDate
            let calendar = Calendar.current
            
            let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
            
            return ("\(ageComponents.year!)")
        }
        
        return "None"
    }
    
    var dobString : String {
        if birthday.lowercased() == "unknown" {
            return "-"
        }
        else {
            return "\(birthday) (\(age))"
        }
    }
    
}


