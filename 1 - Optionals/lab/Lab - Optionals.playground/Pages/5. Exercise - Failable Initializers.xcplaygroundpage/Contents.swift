/*:
 ## Exercise - Failable Initializers
 
 Create a `Computer` struct with two properties, `ram` and `yearManufactured`, where both parameters are of type `Int`. Create a failable initializer that will only create an instance of `Computer` if `ram` is greater than 0, and if `yearManufactured` is greater than 1970, and less than 2017.
 */
struct Computer {
    var ram = 0
    var yearManufactured = 0
    
    init?(rm : Int, year : Int) {
        if rm > 0 {
            ram = rm
        }else {return nil}
        
        if year > 1970 && year < 2017 {
            yearManufactured = year
        }
        else {return nil}
        
    }
}

/*:
 Create two instances of `Computer?` using the failable initializer. One instance should use values that will have a value within the optional, and the other should result in `nil`. Use if-let syntax to unwrap each of the `Computer?` objects and print the `ram` and `yearManufactured` if the optional contains a value.
 */
var comp1 = Computer(rm : 5, year : 2000)
var comp2 = Computer(rm: 10, year: 2020)

if let c1 = comp1 {
    print(c1.ram)
    print(c1.yearManufactured)
}

if let c2 = comp2 {
    print(c2.ram)
    print(c2.yearManufactured)
}
//: [Previous](@previous)  |  page 5 of 6  |  [Next: App Exercise - Workout or Nil](@next)
