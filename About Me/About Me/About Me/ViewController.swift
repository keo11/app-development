//
//  ViewController.swift
//  About Me
//
//  Created by Keo Shiko on 2021/01/29.
//  Copyright © 2021 Glucode. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var changeLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func showMessage(_ sender: Any) {
        changeLabel.text = "I'm a rockstar!"
    }
    
}

