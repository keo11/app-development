import UIKit

struct Icecream : Decodable {
    var flavor = ""
    var type : Form
    var organic : Bool
    
    enum Form : String, Decodable {
        
        case swirl, cone, tub, waterLolly
    }
}

var obj = """
{
    "flavor": "Blueberry Cheesecake",
    "type": "swirl",
    "organic": false
}
"""

var objArray = """

    [
        {"flavor" :  "Vanilla" , "type" : "tub", "organic" : false},
        {"flavor" :  "Chocolate" , "type" : "swirl", "organic" : true}
    ]

"""


func decodeString( obj : String ) -> [Icecream]? {
    
    let json = JSONDecoder()
    let data = obj.data(using: .utf8)!
    
    if let decoded = try? json.decode([Icecream].self, from: data) {
        print(decoded)
        
        return decoded
    }
    
    return nil
}

decodeString(obj: objArray)
