import UIKit

struct Show : Codable {
    var title : String
    var timeSlot : String
    var platform : String
    
    var actors : [Actors]
}

struct Actors : Codable {
    var name = ""
    var surname = ""
    var character = ""
}

func writeToFile(fileName: String, object: [Show]) {
    let plistEncoder = PropertyListEncoder()
    if let encodedNote = try? plistEncoder.encode(object) {
        print(encodedNote)
        let documentsDirectory =
           FileManager.default.urls(for: .documentDirectory,
           in: .userDomainMask).first!
        let archiveURL =
           documentsDirectory.appendingPathComponent(fileName)
        .  appendingPathExtension("plist")
        try? encodedNote.write(to: archiveURL, options: .noFileProtection)
        
        print(archiveURL)
    }
}


let queen = Actors( name: "Connie", surname : "Ferguson", character : "Harriet Khoza")
let brutus = Actors(name: "Themba", surname: "Ndaba", character: "Brutus Shakespear Khoza")
let queenCast = [queen, brutus]

let theQueen = Show(title: "The Queen", timeSlot: "21:00", platform: "Mzansi Magic - DStv - Showmax",actors: queenCast)


//writeToFile(fileName: "Series_extended", object: theQueen)


let beth = Actors( name: "Anya", surname : "Taylor-Joy", character : "Beth Harmon")
let alice = Actors(name: "Chloe", surname: "Pirrie", character: "Alice Harmon")
let gambitCast = [beth, alice]

let gambit = Show(title: "The Queen's Gambit", timeSlot: "00:00", platform: "Netflix and chill",actors: gambitCast)


let series = [gambit, theQueen]

writeToFile(fileName: "Series_extended", object: series)


let encoder = JSONEncoder()
encoder.outputFormatting = .prettyPrinted

let data = try encoder.encode(series)
print(String(data: data, encoding: .utf8)!)


var obj = """
{
    "title": "Optionals in Swift explained: 5 things you should know",
    "url": "https://www.avanderlee.com/swift/optionals-in-swift-explained-5-things-you-should-know/",
    "category": "Swift",
    "views": 47093
}
"""

struct BlogPost : Codable {
    var title = ""
    var url = ""
    var category = ""
    var views = 0
}

func decodeString( obj : String ) -> BlogPost? {
    
    let json = JSONDecoder()
    let data = obj.data(using: .utf8)!
    
    if let decoded = try? json.decode(BlogPost.self, from: data) {
        print(decoded.category)
        
        return decoded
    }
    
    return nil
}

decodeString(obj: obj)
