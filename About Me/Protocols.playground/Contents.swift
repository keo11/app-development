import UIKit

class Series: CustomStringConvertible, Equatable {
    var description: String { return "\(name) first aired in \(year). It is a \(genre) series about \(mainCharacter)" }
    
    static func == (lhs: Series, rhs: Series) -> Bool {
        return lhs.genre == rhs.genre && lhs.year == rhs.year && lhs.mainCharacter == rhs.mainCharacter && lhs.name == rhs.name
    }
    
    var name = ""
    var year = Int()
    var genre = ""
    var mainCharacter = ""
    
    init(name : String, year : Int, genre : String, character : String) {
        self.name = name
        self.year = year
        self.genre = genre
        self.mainCharacter = character
    }
}


let breakingBad = Series(name: "Breaking Bad", year: 2008, genre: "Crime Drama, Thriller", character: "Walter White AKA Heisenburg")

let gameOfThrones = Series(name: "Game Of Thrones", year: 2011, genre: "Fantasy, Drama", character: "Jon Snow")

let americanGods = Series(name: "American Gods", year: 2017, genre: "Fantasy, Drama", character: "Shadow Moon")

print(breakingBad)
print(americanGods.description)

print(breakingBad == gameOfThrones)

if breakingBad == gameOfThrones {
    print("Blasphemy")
}
else {
    print("breaking bad is way better")
}
